# Define target output
target: bit
#
# # Define Firmware Version Number
export PRJ_VERSION = 0x00000001
#
export PROJECT = top
export VIVADO_PROJECT_SIM = tb_top
export VIVADO_PROJECT_SIM_TIME = 1100 ns # Simulation time with unit
#
# # Define target part
export PRJ_PART = xcvu9p-flga2104-2L-e

export PARALLEL_SYNTH = 6

#  # Using a non-standard target directory structure,
# which requires me to define the TOP_DIR path
export TOP_DIR = $(abspath $(PWD))

export MODULES = $(TOP_DIR)/

# # Use top level makefile
include $(TOP_DIR)/ruckus/system_vivado.mk
