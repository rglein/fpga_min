############################################################################
### VCU118 Rev2.0 XDC 12/08/2017
############################################################################

# GPIO
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_SW_*}]
set_property PACKAGE_PIN BB24 [get_ports GPIO_SW_N]
set_property PACKAGE_PIN BF22 [get_ports GPIO_SW_W]
set_property PACKAGE_PIN BE22 [get_ports GPIO_SW_S]
set_property PACKAGE_PIN BE23 [get_ports GPIO_SW_E]
set_property PACKAGE_PIN BD23 [get_ports GPIO_SW_C]
set_property IOSTANDARD LVCMOS12 [get_ports {GPIO_DIP_SW*}]
set_property PACKAGE_PIN B17 [get_ports {GPIO_DIP_SW[0]}]
set_property PACKAGE_PIN G16 [get_ports {GPIO_DIP_SW[1]}]
set_property PACKAGE_PIN J16 [get_ports {GPIO_DIP_SW[2]}]
set_property PACKAGE_PIN D21 [get_ports {GPIO_DIP_SW[3]}]
set_property IOSTANDARD LVCMOS12 [get_ports CPU_RESET]
set_property PACKAGE_PIN L19 [get_ports CPU_RESET]

# LED
set_property IOSTANDARD LVCMOS12 [get_ports {GPIO_LED*}]
set_property PACKAGE_PIN AT32 [get_ports {GPIO_LED[0]}]
set_property PACKAGE_PIN AV34 [get_ports {GPIO_LED[1]}]
set_property PACKAGE_PIN AY30 [get_ports {GPIO_LED[2]}]
set_property PACKAGE_PIN BB32 [get_ports {GPIO_LED[3]}]
set_property PACKAGE_PIN BF32 [get_ports {GPIO_LED[4]}]
set_property PACKAGE_PIN AU37 [get_ports {GPIO_LED[5]}]
set_property PACKAGE_PIN AV36 [get_ports {GPIO_LED[6]}]
set_property PACKAGE_PIN BA37 [get_ports {GPIO_LED[7]}]

# Clock
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports {PIN_250MHZ_CLK1_*}]
set_property PACKAGE_PIN E12 [get_ports PIN_250MHZ_CLK1_P]
set_property PACKAGE_PIN D12 [get_ports PIN_250MHZ_CLK1_N]
set_property IOSTANDARD DIFF_SSTL12 [get_ports {SYSCLK1_300_*}]
set_property PACKAGE_PIN G31      [get_ports "SYSCLK1_300_P"] ;# Bank  47 VCCO - VCC1V2_FPGA - IO_L13P_T2L_N0_GC_QBC_47
set_property PACKAGE_PIN F31      [get_ports "SYSCLK1_300_N"] ;# Bank  47 VCCO - VCC1V2_FPGA - IO_L13N_T2L_N1_GC_QBC_47

# UART
set_property IOSTANDARD LVCMOS18 [get_ports {USB_UART_*}]
set_property PACKAGE_PIN AW25     [get_ports "USB_UART_TX"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L9P_T1L_N4_AD12P_64
set_property PACKAGE_PIN BB21     [get_ports "USB_UART_RX"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L8N_T1L_N3_AD5N_64
#set_property PACKAGE_PIN AY25     [get_ports "USB_UART_RTS"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L9N_T1L_N5_AD12N_64
#set_property PACKAGE_PIN BB22     [get_ports "USB_UART_CTS"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L8P_T1L_N2_AD5P_64

## FireFly GTY
#set_property PACKAGE_PIN C5 [get_ports FIREFLY_TX4_P]
#set_property PACKAGE_PIN D2 [get_ports FIREFLY_RX4_P]
#set_property PACKAGE_PIN D1 [get_ports FIREFLY_RX4_N]
#set_property PACKAGE_PIN C4 [get_ports FIREFLY_TX4_N]
#set_property PACKAGE_PIN E5 [get_ports FIREFLY_TX3_P]
#set_property PACKAGE_PIN F2 [get_ports FIREFLY_RX3_P]
#set_property PACKAGE_PIN F1 [get_ports FIREFLY_RX3_N]
#set_property PACKAGE_PIN E4 [get_ports FIREFLY_TX3_N]
#set_property PACKAGE_PIN F7 [get_ports FIREFLY_TX2_P]
#set_property PACKAGE_PIN H2 [get_ports FIREFLY_RX2_P]
#set_property PACKAGE_PIN H1 [get_ports FIREFLY_RX2_N]
#set_property PACKAGE_PIN F6 [get_ports FIREFLY_TX2_N]
#set_property PACKAGE_PIN G5 [get_ports FIREFLY_TX1_P]
#set_property PACKAGE_PIN K2 [get_ports FIREFLY_RX1_P]
#set_property PACKAGE_PIN K1 [get_ports FIREFLY_RX1_N]
#set_property PACKAGE_PIN G4 [get_ports FIREFLY_TX1_N]

# SGMII Ethernet
set_property PACKAGE_PIN AR23     [get_ports "PHY1_MDIO"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L17P_T2U_N8_AD10P_64
set_property IOSTANDARD  LVCMOS18 [get_ports "PHY1_MDIO"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L17P_T2U_N8_AD10P_64
set_property PACKAGE_PIN AV23     [get_ports "PHY1_MDC"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L14N_T2L_N3_GC_64
set_property IOSTANDARD  LVCMOS18 [get_ports "PHY1_MDC"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L14N_T2L_N3_GC_64
set_property PACKAGE_PIN BA21     [get_ports "PHY1_RESET_B"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_T1U_N12_64
set_property IOSTANDARD  LVCMOS18 [get_ports "PHY1_RESET_B"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_T1U_N12_6
#set_property PACKAGE_PIN AR24     [get_ports "PHY1_PDWN_B_I_INT_B_O"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L18P_T2U_N10_AD2P_64
#set_property IOSTANDARD  LVCMOS18 [get_ports "PHY1_PDWN_B_I_INT_B_O"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L18P_T2U_N10_AD2P_64
#set_property PACKAGE_PIN AR22     [get_ports "PHY1_GPIO_0"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L17N_T2U_N9_AD10N_64
#set_property IOSTANDARD  LVCMOS18 [get_ports "PHY1_GPIO_0"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L17N_T2U_N9_AD10N_64
#set_property PACKAGE_PIN AU23     [get_ports "PHY1_CLKOUT"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L14P_T2L_N2_GC_64
#set_property IOSTANDARD  LVCMOS18 [get_ports "PHY1_CLKOUT"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L14P_T2L_N2_GC_64
set_property IOSTANDARD LVDS [get_ports {PHY1_SGMII_*}]
set_property PACKAGE_PIN AV24     [get_ports "PHY1_SGMII_OUT_N"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L16N_T2U_N7_QBC_AD3N_64
set_property PACKAGE_PIN AU24     [get_ports "PHY1_SGMII_OUT_P"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L16P_T2U_N6_QBC_AD3P_64
#set_property PACKAGE_PIN AV21     [get_ports "PHY1_SGMII_IN_N"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L15N_T2L_N5_AD11N_64
#set_property PACKAGE_PIN AU21     [get_ports "PHY1_SGMII_IN_P"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L15P_T2L_N4_AD11P_64
set_property PACKAGE_PIN AU22     [get_ports "PHY1_SGMII_CLK_N"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L13N_T2L_N1_GC_QBC_64
set_property PACKAGE_PIN AT22     [get_ports "PHY1_SGMII_CLK_P"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L13P_T2L_N0_GC_QBC_64

# FireFly GTY
#set_property PACKAGE_PIN J9 [get_ports "MGT233_CLK1_P"] ;# Bank 233 - MGTREFCLK1P_233
#set_property PACKAGE_PIN J8 [get_ports "MGT233_CLK1_N"] ;# Bank 233 - MGTREFCLK1N_233
#set_property PACKAGE_PIN L9 [get_ports "MGT_SI570_CLOCK3_C_P"] ;# Bank 233 - MGTREFCLK0P_233
#set_property PACKAGE_PIN L8 [get_ports "MGT_SI570_CLOCK3_C_N"] ;# Bank 233 - MGTREFCLK0N_233
#set_property PACKAGE_PIN C5 [get_ports FIREFLY_TX4_P] ;# Bank 233 - MGTYTXP3_233
#set_property PACKAGE_PIN D2 [get_ports FIREFLY_RX4_P] ;# Bank 233 - MGTYRXP3_233
#set_property PACKAGE_PIN D1 [get_ports FIREFLY_RX4_N] ;# Bank 233 - MGTYRXN3_233
#set_property PACKAGE_PIN C4 [get_ports FIREFLY_TX4_N] ;# Bank 233 - MGTYTXN3_233
#set_property PACKAGE_PIN E5 [get_ports FIREFLY_TX3_P] ;# Bank 233 - MGTYTXP2_233
#set_property PACKAGE_PIN F2 [get_ports FIREFLY_RX3_P] ;# Bank 233 - MGTYRXP2_233
#set_property PACKAGE_PIN F1 [get_ports FIREFLY_RX3_N] ;# Bank 233 - MGTYRXN2_233
#set_property PACKAGE_PIN E4 [get_ports FIREFLY_TX3_N] ;# Bank 233 - MGTYTXN2_233
#set_property PACKAGE_PIN F7 [get_ports FIREFLY_TX2_P] ;# Bank 233 - MGTYTXP1_233
#set_property PACKAGE_PIN H2 [get_ports FIREFLY_RX2_P] ;# Bank 233 - MGTYRXP1_233
#set_property PACKAGE_PIN H1 [get_ports FIREFLY_RX2_N] ;# Bank 233 - MGTYRXN1_233
#set_property PACKAGE_PIN F6 [get_ports FIREFLY_TX2_N] ;# Bank 233 - MGTYTXN1_233
#set_property PACKAGE_PIN G5 [get_ports FIREFLY_TX1_P] ;# Bank 233 - MGTYTXP0_233
#set_property PACKAGE_PIN K2 [get_ports FIREFLY_RX1_P] ;# Bank 233 - MGTYRXP0_233
#set_property PACKAGE_PIN K1 [get_ports FIREFLY_RX1_N] ;# Bank 233 - MGTYRXN0_233
#set_property PACKAGE_PIN G4 [get_ports FIREFLY_TX1_N] ;# Bank 233 - MGTYTXN0_233
## FMCP (Samtec) GTY 121
#set_property PACKAGE_PIN AK39 [get_ports "FMCP_HSPC_GBT0_0_N"] ;# Bank 121 - MGTREFCLK0N_121
#set_property PACKAGE_PIN AK38 [get_ports "FMCP_HSPC_GBT0_0_P"] ;# Bank 121 - MGTREFCLK0P_121
#set_property PACKAGE_PIN AH39 [get_ports "FMCP_HSPC_GBT1_0_N"] ;# Bank 121 - MGTREFCLK1N_121
#set_property PACKAGE_PIN AH38 [get_ports "FMCP_HSPC_GBT1_0_P"] ;# Bank 121 - MGTREFCLK1P_121
#set_property PACKAGE_PIN AL41 [get_ports "FMCP_HSPC_DP3_C2M_N"] ;# Bank 121 - MGTYTXN3_121
#set_property PACKAGE_PIN AL40 [get_ports "FMCP_HSPC_DP3_C2M_P"] ;# Bank 121 - MGTYTXP3_121
#set_property PACKAGE_PIN AJ45 [get_ports "FMCP_HSPC_DP3_M2C_P"] ;# Bank 121 - MGTYRXP3_121
#set_property PACKAGE_PIN AJ46 [get_ports "FMCP_HSPC_DP3_M2C_N"] ;# Bank 121 - MGTYRXN3_121
#set_property PACKAGE_PIN AM43 [get_ports "FMCP_HSPC_DP2_C2M_N"] ;# Bank 121 - MGTYTXN2_121
#set_property PACKAGE_PIN AM42 [get_ports "FMCP_HSPC_DP2_C2M_P"] ;# Bank 121 - MGTYTXP2_121
#set_property PACKAGE_PIN AL45 [get_ports "FMCP_HSPC_DP2_M2C_P"] ;# Bank 121 - MGTYRXP2_121
#set_property PACKAGE_PIN AL46 [get_ports "FMCP_HSPC_DP2_M2C_N"] ;# Bank 121 - MGTYRXN2_121
#set_property PACKAGE_PIN AP43 [get_ports "FMCP_HSPC_DP1_C2M_N"] ;# Bank 121 - MGTYTXN1_121
#set_property PACKAGE_PIN AP42 [get_ports "FMCP_HSPC_DP1_C2M_P"] ;# Bank 121 - MGTYTXP1_121
#set_property PACKAGE_PIN AN45 [get_ports "FMCP_HSPC_DP1_M2C_P"] ;# Bank 121 - MGTYRXP1_121
#set_property PACKAGE_PIN AN46 [get_ports "FMCP_HSPC_DP1_M2C_N"] ;# Bank 121 - MGTYRXN1_121
#set_property PACKAGE_PIN AT43 [get_ports "FMCP_HSPC_DP0_C2M_N"] ;# Bank 121 - MGTYTXN0_121
#set_property PACKAGE_PIN AT42 [get_ports "FMCP_HSPC_DP0_C2M_P"] ;# Bank 121 - MGTYTXP0_121
#set_property PACKAGE_PIN AR45 [get_ports "FMCP_HSPC_DP0_M2C_P"] ;# Bank 121 - MGTYRXP0_121
#set_property PACKAGE_PIN AR46 [get_ports "FMCP_HSPC_DP0_M2C_N"] ;# Bank 121 - MGTYRXN0_121
## FMCP (Samtec) GTY 122
#set_property PACKAGE_PIN AF39 [get_ports "FMCP_HSPC_GBTCLK2_M2C_C_N"] ;# Bank 122 - MGTREFCLK0N_122
#set_property PACKAGE_PIN AF38 [get_ports "FMCP_HSPC_GBTCLK2_M2C_C_P"] ;# Bank 122 - MGTREFCLK0P_122
#set_property PACKAGE_PIN AD39 [get_ports "FMCP_HSPC_GBT1_2_N"] ;# Bank 122 - MGTREFCLK1N_122
#set_property PACKAGE_PIN AD38 [get_ports "FMCP_HSPC_GBT1_2_P"] ;# Bank 122 - MGTREFCLK1P_122
#set_property PACKAGE_PIN AE41 [get_ports "FMCP_HSPC_DP11_C2M_N"] ;# Bank 122 - MGTYTXN3_122
#set_property PACKAGE_PIN AE40 [get_ports "FMCP_HSPC_DP11_C2M_P"] ;# Bank 122 - MGTYTXP3_122
#set_property PACKAGE_PIN AD43 [get_ports "FMCP_HSPC_DP11_M2C_P"] ;# Bank 122 - MGTYRXP3_122
#set_property PACKAGE_PIN AD44 [get_ports "FMCP_HSPC_DP11_M2C_N"] ;# Bank 122 - MGTYRXN3_122
#set_property PACKAGE_PIN AG41 [get_ports "FMCP_HSPC_DP10_C2M_N"] ;# Bank 122 - MGTYTXN2_122
#set_property PACKAGE_PIN AG40 [get_ports "FMCP_HSPC_DP10_C2M_P"] ;# Bank 122 - MGTYTXP2_122
#set_property PACKAGE_PIN AE45 [get_ports "FMCP_HSPC_DP10_M2C_P"] ;# Bank 122 - MGTYRXP2_122
#set_property PACKAGE_PIN AE46 [get_ports "FMCP_HSPC_DP10_M2C_N"] ;# Bank 122 - MGTYRXN2_122
#set_property PACKAGE_PIN AJ41 [get_ports "FMCP_HSPC_DP9_C2M_N"] ;# Bank 122 - MGTYTXN1_122
#set_property PACKAGE_PIN AJ40 [get_ports "FMCP_HSPC_DP9_C2M_P"] ;# Bank 122 - MGTYTXP1_122
#set_property PACKAGE_PIN AF43 [get_ports "FMCP_HSPC_DP9_M2C_P"] ;# Bank 122 - MGTYRXP1_122
#set_property PACKAGE_PIN AF44 [get_ports "FMCP_HSPC_DP9_M2C_N"] ;# Bank 122 - MGTYRXN1_122
#set_property PACKAGE_PIN AK43 [get_ports "FMCP_HSPC_DP8_C2M_N"] ;# Bank 122 - MGTYTXN0_122
#set_property PACKAGE_PIN AK42 [get_ports "FMCP_HSPC_DP8_C2M_P"] ;# Bank 122 - MGTYTXP0_122
#set_property PACKAGE_PIN AG45 [get_ports "FMCP_HSPC_DP8_M2C_P"] ;# Bank 122 - MGTYRXP0_122
#set_property PACKAGE_PIN AG46 [get_ports "FMCP_HSPC_DP8_M2C_N"] ;# Bank 122 - MGTYRXN0_122

# DDR CS 1
set_property PACKAGE_PIN B20      [get_ports {DDR4_C1_DQ[39]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L24N_T3U_N11_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[39]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L24N_T3U_N11_73
set_property PACKAGE_PIN C20      [get_ports {DDR4_C1_DQ[38]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L24P_T3U_N10_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[38]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L24P_T3U_N10_73
set_property PACKAGE_PIN D19      [get_ports {DDR4_C1_DQ[37]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L23N_T3U_N9_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[37]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L23N_T3U_N9_73
set_property PACKAGE_PIN D20      [get_ports {DDR4_C1_DQ[36]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L23P_T3U_N8_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[36]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L23P_T3U_N8_73
set_property PACKAGE_PIN A18      [get_ports {DDR4_C1_DQS_C[4]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L22N_T3U_N7_DBC_AD0N_73
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[4]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L22N_T3U_N7_DBC_AD0N_73
set_property PACKAGE_PIN A19      [get_ports {DDR4_C1_DQS_T[4]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L22P_T3U_N6_DBC_AD0P_73
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[4]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L22P_T3U_N6_DBC_AD0P_73
set_property PACKAGE_PIN C18      [get_ports {DDR4_C1_DQ[35]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L21N_T3L_N5_AD8N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[35]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L21N_T3L_N5_AD8N_73
set_property PACKAGE_PIN C19      [get_ports {DDR4_C1_DQ[34]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L21P_T3L_N4_AD8P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[34]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L21P_T3L_N4_AD8P_73
set_property PACKAGE_PIN C17      [get_ports {DDR4_C1_DQ[33]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L20N_T3L_N3_AD1N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[33]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L20N_T3L_N3_AD1N_73
set_property PACKAGE_PIN D17      [get_ports {DDR4_C1_DQ[32]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L20P_T3L_N2_AD1P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[32]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L20P_T3L_N2_AD1P_73
set_property PACKAGE_PIN B18      [get_ports {DDR4_C1_DM[4]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L19P_T3L_N0_DBC_AD9P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[4]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L19P_T3L_N0_DBC_AD9P_73
#set_property PACKAGE_PIN A20      [get_ports "DDR4_C1_TEN"] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_T3U_N12_73
#set_property IOSTANDARD  POD12_DCI [get_ports "DDR4_C1_TEN"] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_T3U_N12_73
set_property PACKAGE_PIN D16      [get_ports {DDR4_C1_DQ[31]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L18N_T2U_N11_AD2N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[31]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L18N_T2U_N11_AD2N_73
set_property PACKAGE_PIN E17      [get_ports {DDR4_C1_DQ[30]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L18P_T2U_N10_AD2P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[30]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L18P_T2U_N10_AD2P_73
set_property PACKAGE_PIN F20      [get_ports {DDR4_C1_DQ[29]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L17N_T2U_N9_AD10N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[29]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L17N_T2U_N9_AD10N_73
set_property PACKAGE_PIN G20      [get_ports {DDR4_C1_DQ[28]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L17P_T2U_N8_AD10P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[28]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L17P_T2U_N8_AD10P_73
set_property PACKAGE_PIN E16      [get_ports {DDR4_C1_DQS_C[3]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L16N_T2U_N7_QBC_AD3N_73
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[3]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L16N_T2U_N7_QBC_AD3N_73
set_property PACKAGE_PIN F16      [get_ports {DDR4_C1_DQS_T[3]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L16P_T2U_N6_QBC_AD3P_73
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[3]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L16P_T2U_N6_QBC_AD3P_73
set_property PACKAGE_PIN E18      [get_ports {DDR4_C1_DQ[27]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L15N_T2L_N5_AD11N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[27]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L15N_T2L_N5_AD11N_73
set_property PACKAGE_PIN E19      [get_ports {DDR4_C1_DQ[26]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L15P_T2L_N4_AD11P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[26]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L15P_T2L_N4_AD11P_73
set_property PACKAGE_PIN F18      [get_ports {DDR4_C1_DQ[25]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L14N_T2L_N3_GC_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[25]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L14N_T2L_N3_GC_73
set_property PACKAGE_PIN F19      [get_ports {DDR4_C1_DQ[24]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L14P_T2L_N2_GC_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[24]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L14P_T2L_N2_GC_73
set_property PACKAGE_PIN G18      [get_ports {DDR4_C1_DM[3]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L13P_T2L_N0_GC_QBC_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[3]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L13P_T2L_N0_GC_QBC_73
set_property PACKAGE_PIN H18      [get_ports {DDR4_C1_DQ[23]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L12N_T1U_N11_GC_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[23]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L12N_T1U_N11_GC_73
set_property PACKAGE_PIN H19      [get_ports {DDR4_C1_DQ[22]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L12P_T1U_N10_GC_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[22]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L12P_T1U_N10_GC_73
set_property PACKAGE_PIN H17      [get_ports {DDR4_C1_DQ[21]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L11N_T1U_N9_GC_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[21]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L11N_T1U_N9_GC_73
set_property PACKAGE_PIN J17      [get_ports {DDR4_C1_DQ[20]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L11P_T1U_N8_GC_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[20]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L11P_T1U_N8_GC_73
set_property PACKAGE_PIN J19      [get_ports {DDR4_C1_DQS_C[2]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L10N_T1U_N7_QBC_AD4N_73
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[2]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L10N_T1U_N7_QBC_AD4N_73
set_property PACKAGE_PIN K19      [get_ports {DDR4_C1_DQS_T[2]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L10P_T1U_N6_QBC_AD4P_73
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[2]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L10P_T1U_N6_QBC_AD4P_73
set_property PACKAGE_PIN K18      [get_ports {DDR4_C1_DQ[19]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L9N_T1L_N5_AD12N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[19]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L9N_T1L_N5_AD12N_73
set_property PACKAGE_PIN L18      [get_ports {DDR4_C1_DQ[18]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L9P_T1L_N4_AD12P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[18]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L9P_T1L_N4_AD12P_73
set_property PACKAGE_PIN K16      [get_ports {DDR4_C1_DQ[17]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L8N_T1L_N3_AD5N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[17]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L8N_T1L_N3_AD5N_73
set_property PACKAGE_PIN L16      [get_ports {DDR4_C1_DQ[16]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L8P_T1L_N2_AD5P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[16]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L8P_T1L_N2_AD5P_73
set_property PACKAGE_PIN K17      [get_ports {DDR4_C1_DM[2]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L7P_T1L_N0_QBC_AD13P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[2]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L7P_T1L_N0_QBC_AD13P_73
set_property PACKAGE_PIN M16      [get_ports {DDR4_C1_DQ[15]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L6N_T0U_N11_AD6N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[15]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L6N_T0U_N11_AD6N_73
set_property PACKAGE_PIN N17      [get_ports {DDR4_C1_DQ[14]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L6P_T0U_N10_AD6P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[14]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L6P_T0U_N10_AD6P_73
set_property PACKAGE_PIN N18      [get_ports {DDR4_C1_DQ[13]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L5N_T0U_N9_AD14N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[13]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L5N_T0U_N9_AD14N_73
set_property PACKAGE_PIN N19      [get_ports {DDR4_C1_DQ[12]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L5P_T0U_N8_AD14P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[12]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L5P_T0U_N8_AD14P_73
set_property PACKAGE_PIN P16      [get_ports {DDR4_C1_DQS_C[1]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L4N_T0U_N7_DBC_AD7N_73
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[1]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L4N_T0U_N7_DBC_AD7N_73
set_property PACKAGE_PIN P17      [get_ports {DDR4_C1_DQS_T[1]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L4P_T0U_N6_DBC_AD7P_73
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[1]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L4P_T0U_N6_DBC_AD7P_73
set_property PACKAGE_PIN M17      [get_ports {DDR4_C1_DQ[11]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L3N_T0L_N5_AD15N_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[11]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L3N_T0L_N5_AD15N_73
set_property PACKAGE_PIN M18      [get_ports {DDR4_C1_DQ[10]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L3P_T0L_N4_AD15P_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[10]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L3P_T0L_N4_AD15P_73
set_property PACKAGE_PIN P19      [get_ports {DDR4_C1_DQ[9]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L2N_T0L_N3_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[9]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L2N_T0L_N3_73
set_property PACKAGE_PIN R19      [get_ports {DDR4_C1_DQ[8]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L2P_T0L_N2_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[8]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L2P_T0L_N2_73
#set_property PACKAGE_PIN R17      [get_ports "DDR4_C1_ALERT_B"] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L1N_T0L_N1_DBC_73
#set_property IOSTANDARD  POD12_DCI [get_ports "DDR4_C1_ALERT_B"] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L1N_T0L_N1_DBC_73
set_property PACKAGE_PIN R18      [get_ports {DDR4_C1_DM[1]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L1P_T0L_N0_DBC_73
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[1]}] ;# Bank  73 VCCO - VCC1V2_FPGA - IO_L1P_T0L_N0_DBC_73
#set_property PACKAGE_PIN A21      [get_ports {DDR4_C1_DQ[71]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L24N_T3U_N11_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[71]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L24N_T3U_N11_72
#set_property PACKAGE_PIN B21      [get_ports {DDR4_C1_DQ[70]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L24P_T3U_N10_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[70]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L24P_T3U_N10_72
#set_property PACKAGE_PIN B22      [get_ports {DDR4_C1_DQ[69]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L23N_T3U_N9_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[69]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L23N_T3U_N9_72
#set_property PACKAGE_PIN B23      [get_ports {DDR4_C1_DQ[68]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L23P_T3U_N8_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[68]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L23P_T3U_N8_72
#set_property PACKAGE_PIN C22      [get_ports {DDR4_C1_DQS_C[8]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L22N_T3U_N7_DBC_AD0N_72
#set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[8]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L22N_T3U_N7_DBC_AD0N_72
#set_property PACKAGE_PIN D22      [get_ports {DDR4_C1_DQS_T[8]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L22P_T3U_N6_DBC_AD0P_72
#set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[8]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L22P_T3U_N6_DBC_AD0P_72
#set_property PACKAGE_PIN C23      [get_ports {DDR4_C1_DQ[67]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L21N_T3L_N5_AD8N_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[67]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L21N_T3L_N5_AD8N_72
#set_property PACKAGE_PIN C24      [get_ports {DDR4_C1_DQ[66]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L21P_T3L_N4_AD8P_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[66]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L21P_T3L_N4_AD8P_72
#set_property PACKAGE_PIN A23      [get_ports {DDR4_C1_DQ[65]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L20N_T3L_N3_AD1N_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[65]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L20N_T3L_N3_AD1N_72
#set_property PACKAGE_PIN A24      [get_ports {DDR4_C1_DQ[64]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L20P_T3L_N2_AD1P_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[64]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L20P_T3L_N2_AD1P_72
#set_property PACKAGE_PIN E24      [get_ports {DDR4_C1_DM[8]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L19P_T3L_N0_DBC_AD9P_72
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[8]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L19P_T3L_N0_DBC_AD9P_72
set_property PACKAGE_PIN F23      [get_ports {DDR4_C1_DQ[63]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L18N_T2U_N11_AD2N_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[63]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L18N_T2U_N11_AD2N_72
set_property PACKAGE_PIN F24      [get_ports {DDR4_C1_DQ[62]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L18P_T2U_N10_AD2P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[62]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L18P_T2U_N10_AD2P_72
set_property PACKAGE_PIN E21      [get_ports {DDR4_C1_DQ[61]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L17N_T2U_N9_AD10N_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[61]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L17N_T2U_N9_AD10N_72
set_property PACKAGE_PIN F21      [get_ports {DDR4_C1_DQ[60]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L17P_T2U_N8_AD10P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[60]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L17P_T2U_N8_AD10P_72
set_property PACKAGE_PIN G23      [get_ports {DDR4_C1_DQS_C[7]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L16N_T2U_N7_QBC_AD3N_72
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[7]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L16N_T2U_N7_QBC_AD3N_72
set_property PACKAGE_PIN H24      [get_ports {DDR4_C1_DQS_T[7]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L16P_T2U_N6_QBC_AD3P_72
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[7]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L16P_T2U_N6_QBC_AD3P_72
set_property PACKAGE_PIN E22      [get_ports {DDR4_C1_DQ[59]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L15N_T2L_N5_AD11N_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[59]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L15N_T2L_N5_AD11N_72
set_property PACKAGE_PIN E23      [get_ports {DDR4_C1_DQ[58]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L15P_T2L_N4_AD11P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[58]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L15P_T2L_N4_AD11P_72
set_property PACKAGE_PIN H22      [get_ports {DDR4_C1_DQ[57]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L14N_T2L_N3_GC_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[57]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L14N_T2L_N3_GC_72
set_property PACKAGE_PIN H23      [get_ports {DDR4_C1_DQ[56]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L14P_T2L_N2_GC_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[56]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L14P_T2L_N2_GC_72
set_property PACKAGE_PIN G22      [get_ports {DDR4_C1_DM[7]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L13P_T2L_N0_GC_QBC_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[7]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L13P_T2L_N0_GC_QBC_72
set_property PACKAGE_PIN J22      [get_ports {DDR4_C1_DQ[55]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L12N_T1U_N11_GC_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[55]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L12N_T1U_N11_GC_72
set_property PACKAGE_PIN K22      [get_ports {DDR4_C1_DQ[54]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L12P_T1U_N10_GC_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[54]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L12P_T1U_N10_GC_72
set_property PACKAGE_PIN J21      [get_ports {DDR4_C1_DQ[53]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L11N_T1U_N9_GC_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[53]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L11N_T1U_N9_GC_72
set_property PACKAGE_PIN K21      [get_ports {DDR4_C1_DQ[52]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L11P_T1U_N8_GC_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[52]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L11P_T1U_N8_GC_72
set_property PACKAGE_PIN L20      [get_ports {DDR4_C1_DQS_C[6]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L10N_T1U_N7_QBC_AD4N_72
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[6]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L10N_T1U_N7_QBC_AD4N_72
set_property PACKAGE_PIN M20      [get_ports {DDR4_C1_DQS_T[6]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L10P_T1U_N6_QBC_AD4P_72
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[6]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L10P_T1U_N6_QBC_AD4P_72
set_property PACKAGE_PIN L21      [get_ports {DDR4_C1_DQ[51]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L9N_T1L_N5_AD12N_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[51]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L9N_T1L_N5_AD12N_72
set_property PACKAGE_PIN M21      [get_ports {DDR4_C1_DQ[50]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L9P_T1L_N4_AD12P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[50]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L9P_T1L_N4_AD12P_72
set_property PACKAGE_PIN J24      [get_ports {DDR4_C1_DQ[49]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L8N_T1L_N3_AD5N_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[49]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L8N_T1L_N3_AD5N_72
set_property PACKAGE_PIN K24      [get_ports {DDR4_C1_DQ[48]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L8P_T1L_N2_AD5P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[48]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L8P_T1L_N2_AD5P_72
set_property PACKAGE_PIN L23      [get_ports {DDR4_C1_DM[6]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L7P_T1L_N0_QBC_AD13P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[6]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L7P_T1L_N0_QBC_AD13P_72
set_property PACKAGE_PIN R23      [get_ports {DDR4_C1_DQ[47]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L6N_T0U_N11_AD6N_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[47]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L6N_T0U_N11_AD6N_72
set_property PACKAGE_PIN T23      [get_ports {DDR4_C1_DQ[46]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L6P_T0U_N10_AD6P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[46]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L6P_T0U_N10_AD6P_72
set_property PACKAGE_PIN P22      [get_ports {DDR4_C1_DQ[45]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L5N_T0U_N9_AD14N_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[45]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L5N_T0U_N9_AD14N_72
set_property PACKAGE_PIN R22      [get_ports {DDR4_C1_DQ[44]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L5P_T0U_N8_AD14P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[44]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L5P_T0U_N8_AD14P_72
set_property PACKAGE_PIN M22      [get_ports {DDR4_C1_DQS_C[5]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L4N_T0U_N7_DBC_AD7N_72
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[5]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L4N_T0U_N7_DBC_AD7N_72
set_property PACKAGE_PIN N22      [get_ports {DDR4_C1_DQS_T[5]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L4P_T0U_N6_DBC_AD7P_72
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[5]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L4P_T0U_N6_DBC_AD7P_72
set_property PACKAGE_PIN P21      [get_ports {DDR4_C1_DQ[43]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L3N_T0L_N5_AD15N_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[43]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L3N_T0L_N5_AD15N_72
set_property PACKAGE_PIN R21      [get_ports {DDR4_C1_DQ[42]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L3P_T0L_N4_AD15P_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[42]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L3P_T0L_N4_AD15P_72
set_property PACKAGE_PIN M23      [get_ports {DDR4_C1_DQ[41]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L2N_T0L_N3_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[41]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L2N_T0L_N3_72
set_property PACKAGE_PIN N23      [get_ports {DDR4_C1_DQ[40]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L2P_T0L_N2_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[40]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L2P_T0L_N2_72
set_property PACKAGE_PIN N20      [get_ports "DDR4_C1_RESET_B"] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L1N_T0L_N1_DBC_72
set_property IOSTANDARD  LVCMOS12 [get_ports "DDR4_C1_RESET_B"] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L1N_T0L_N1_DBC_72
set_property PACKAGE_PIN P20      [get_ports {DDR4_C1_DM[5]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L1P_T0L_N0_DBC_72
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[5]}] ;# Bank  72 VCCO - VCC1V2_FPGA - IO_L1P_T0L_N0_DBC_72
set_property PACKAGE_PIN B15      [get_ports {DDR4_C1_A[1]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L24N_T3U_N11_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[1]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L24N_T3U_N11_71
set_property PACKAGE_PIN B16      [get_ports {DDR4_C1_A[2]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L24P_T3U_N10_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[2]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L24P_T3U_N10_71
set_property PACKAGE_PIN C14      [get_ports {DDR4_C1_A[3]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L23N_T3U_N9_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[3]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L23N_T3U_N9_71
set_property PACKAGE_PIN C15      [get_ports {DDR4_C1_A[4]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L23P_T3U_N8_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[4]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L23P_T3U_N8_71
set_property PACKAGE_PIN A13      [get_ports {DDR4_C1_A[5]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L22N_T3U_N7_DBC_AD0N_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[5]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L22N_T3U_N7_DBC_AD0N_71
set_property PACKAGE_PIN A14      [get_ports {DDR4_C1_A[6]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L22P_T3U_N6_DBC_AD0P_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[6]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L22P_T3U_N6_DBC_AD0P_71
set_property PACKAGE_PIN A15      [get_ports {DDR4_C1_A[7]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L21N_T3L_N5_AD8N_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[7]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L21N_T3L_N5_AD8N_71
set_property PACKAGE_PIN A16      [get_ports {DDR4_C1_A[8]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L21P_T3L_N4_AD8P_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[8]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L21P_T3L_N4_AD8P_71
set_property PACKAGE_PIN B12      [get_ports {DDR4_C1_A[9]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L20N_T3L_N3_AD1N_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[9]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L20N_T3L_N3_AD1N_71
set_property PACKAGE_PIN C12      [get_ports {DDR4_C1_A[10]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L20P_T3L_N2_AD1P_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[10]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L20P_T3L_N2_AD1P_71
set_property PACKAGE_PIN B13      [get_ports {DDR4_C1_A[11]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L19N_T3L_N1_DBC_AD9N_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[11]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L19N_T3L_N1_DBC_AD9N_71
set_property PACKAGE_PIN C13      [get_ports {DDR4_C1_A[12]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L19P_T3L_N0_DBC_AD9P_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[12]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L19P_T3L_N0_DBC_AD9P_71
set_property PACKAGE_PIN D14      [get_ports {DDR4_C1_A[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_T3U_N12_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_T3U_N12_71
set_property PACKAGE_PIN D15      [get_ports {DDR4_C1_A[13]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_T2U_N12_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[13]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_T2U_N12_71
set_property PACKAGE_PIN H14      [get_ports {DDR4_C1_A[14]}] ;#_WE_B]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L18N_T2U_N11_AD2N_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[14]}] ;#_WE_B]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L18N_T2U_N11_AD2N_71
set_property PACKAGE_PIN H15      [get_ports {DDR4_C1_A[15]}] ;#_CAS_B]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L18P_T2U_N10_AD2P_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[15]}] ;#_CAS_B]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L18P_T2U_N10_AD2P_71
set_property PACKAGE_PIN F15      [get_ports {DDR4_C1_A[16]}] ;#_RAS_B]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L17N_T2U_N9_AD10N_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_A[16]}] ;#_RAS_B]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L17N_T2U_N9_AD10N_71
set_property PACKAGE_PIN G15      [get_ports {DDR4_C1_BA[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L17P_T2U_N8_AD10P_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_BA[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L17P_T2U_N8_AD10P_71
#set_property PACKAGE_PIN E14      [get_ports "DDR4_C1_CK_C"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L16N_T2U_N7_QBC_AD3N_71
#set_property IOSTANDARD  DIFF_SSTL12_DCI [get_ports "DDR4_C1_CK_C"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L16N_T2U_N7_QBC_AD3N_71
#set_property PACKAGE_PIN F14      [get_ports "DDR4_C1_CK_T"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L16P_T2U_N6_QBC_AD3P_71
#set_property IOSTANDARD  DIFF_SSTL12_DCI [get_ports "DDR4_C1_CK_T"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L16P_T2U_N6_QBC_AD3P_71
set_property PACKAGE_PIN G13      [get_ports {DDR4_C1_BA[1]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L15N_T2L_N5_AD11N_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_BA[1]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L15N_T2L_N5_AD11N_71
set_property PACKAGE_PIN H13      [get_ports {DDR4_C1_BG0}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L15P_T2L_N4_AD11P_71
set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_C1_BG0}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L15P_T2L_N4_AD11P_71
set_property PACKAGE_PIN E13      [get_ports "DDR4_C1_ACT_B"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L14N_T2L_N3_GC_71
set_property IOSTANDARD  SSTL12_DCI [get_ports "DDR4_C1_ACT_B"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L14N_T2L_N3_GC_71
set_property PACKAGE_PIN F13      [get_ports "DDR4_C1_CS_B"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L14P_T2L_N2_GC_71
set_property IOSTANDARD  SSTL12_DCI [get_ports "DDR4_C1_CS_B"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L14P_T2L_N2_GC_71
#set_property PACKAGE_PIN A11      [get_ports {DDR4_C1_DQ[79]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L12N_T1U_N11_GC_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[79]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L12N_T1U_N11_GC_71
#set_property PACKAGE_PIN B11      [get_ports {DDR4_C1_DQ[78]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L12P_T1U_N10_GC_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[78]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L12P_T1U_N10_GC_71
#set_property PACKAGE_PIN B10      [get_ports {DDR4_C1_DQ[77]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L11N_T1U_N9_GC_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[77]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L11N_T1U_N9_GC_71
#set_property PACKAGE_PIN C10      [get_ports {DDR4_C1_DQ[76]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L11P_T1U_N8_GC_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[76]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L11P_T1U_N8_GC_71
#set_property PACKAGE_PIN A8       [get_ports {DDR4_C1_DQS_C[9]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L10N_T1U_N7_QBC_AD4N_71
#set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[9]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L10N_T1U_N7_QBC_AD4N_71
#set_property PACKAGE_PIN A9       [get_ports {DDR4_C1_DQS_T[9]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L10P_T1U_N6_QBC_AD4P_71
#set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[9]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L10P_T1U_N6_QBC_AD4P_71
#set_property PACKAGE_PIN B7       [get_ports {DDR4_C1_DQ[75]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L9N_T1L_N5_AD12N_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[75]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L9N_T1L_N5_AD12N_71
#set_property PACKAGE_PIN B8       [get_ports {DDR4_C1_DQ[74]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L9P_T1L_N4_AD12P_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[74]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L9P_T1L_N4_AD12P_71
#set_property PACKAGE_PIN C7       [get_ports {DDR4_C1_DQ[73]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L8N_T1L_N3_AD5N_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[73]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L8N_T1L_N3_AD5N_71
#set_property PACKAGE_PIN D7       [get_ports {DDR4_C1_DQ[72]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L8P_T1L_N2_AD5P_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[72]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L8P_T1L_N2_AD5P_71
set_property PACKAGE_PIN C8       [get_ports "DDR4_C1_ODT"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L7N_T1L_N1_QBC_AD13N_71
set_property IOSTANDARD  SSTL12_DCI [get_ports "DDR4_C1_ODT"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L7N_T1L_N1_QBC_AD13N_71
#set_property PACKAGE_PIN C9       [get_ports {DDR4_C1_DM[9]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L7P_T1L_N0_QBC_AD13P_71
#set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[9]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L7P_T1L_N0_QBC_AD13P_71
set_property PACKAGE_PIN A10      [get_ports "DDR4_C1_CKE"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_T1U_N12_71
set_property IOSTANDARD  SSTL12_DCI [get_ports "DDR4_C1_CKE"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_T1U_N12_71
set_property PACKAGE_PIN D9       [get_ports {DDR4_C1_DQ[7]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L6N_T0U_N11_AD6N_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[7]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L6N_T0U_N11_AD6N_71
set_property PACKAGE_PIN E9       [get_ports {DDR4_C1_DQ[6]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L6P_T0U_N10_AD6P_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[6]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L6P_T0U_N10_AD6P_71
set_property PACKAGE_PIN G12      [get_ports {DDR4_C1_DQ[5]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L5N_T0U_N9_AD14N_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[5]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L5N_T0U_N9_AD14N_71
set_property PACKAGE_PIN H12      [get_ports {DDR4_C1_DQ[4]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L5P_T0U_N8_AD14P_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[4]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L5P_T0U_N8_AD14P_71
set_property PACKAGE_PIN D10      [get_ports {DDR4_C1_DQS_C[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L4N_T0U_N7_DBC_AD7N_71
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_C[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L4N_T0U_N7_DBC_AD7N_71
set_property PACKAGE_PIN D11      [get_ports {DDR4_C1_DQS_T[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L4P_T0U_N6_DBC_AD7P_71
set_property IOSTANDARD  DIFF_POD12_DCI [get_ports {DDR4_C1_DQS_T[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L4P_T0U_N6_DBC_AD7P_71
set_property PACKAGE_PIN F9       [get_ports {DDR4_C1_DQ[3]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L3N_T0L_N5_AD15N_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[3]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L3N_T0L_N5_AD15N_71
set_property PACKAGE_PIN F10      [get_ports {DDR4_C1_DQ[2]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L3P_T0L_N4_AD15P_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[2]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L3P_T0L_N4_AD15P_71
set_property PACKAGE_PIN E11      [get_ports {DDR4_C1_DQ[1]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L2N_T0L_N3_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[1]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L2N_T0L_N3_71
set_property PACKAGE_PIN F11      [get_ports {DDR4_C1_DQ[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L2P_T0L_N2_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DQ[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L2P_T0L_N2_71
#set_property PACKAGE_PIN G10      [get_ports "DDR4_C1_PAR"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L1N_T0L_N1_DBC_71
#set_property IOSTANDARD  POD12_DCI [get_ports "DDR4_C1_PAR"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L1N_T0L_N1_DBC_71
set_property PACKAGE_PIN G11      [get_ports {DDR4_C1_DM[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L1P_T0L_N0_DBC_71
set_property IOSTANDARD  POD12_DCI [get_ports {DDR4_C1_DM[0]}] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L1P_T0L_N0_DBC_71
# DDR4 CS2


##############################################################################################################################

# Clock Constraints
create_clock -name clk_300 -period 3.333 -waveform {0.000 1.667} [get_nets sl_Clk300_buf]
create_clock -name PHY1_SGMII_CLK -period 1.600 [get_ports PHY1_SGMII_CLK_P]

# Path Constraints
set_false_path -from [get_clocks -of_objects [get_pins i_clk_wiz_160_250/inst/mmcme4_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins i_clk_wiz_160_250/inst/mmcme4_adv_inst/CLKOUT0]] ;# be very careful with this statement
#set_false_path -from [get_clocks -of_objects [get_pins i_clk_wiz_160_250/inst/mmcme4_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins i_clk_wiz_160_250/inst/mmcme4_adv_inst/CLKOUT1]] ;# be very careful with this statement



# Misc
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]

# Debug
