--==========================================================================
-- CU Boulder
-------------------------------------------------------------------------------
--! @file
--! @brief FPGA universal top file.
--! @author glein
--! @date 2018-05-03
--! @version v.1.0
--! @details
--! This module is the top module.
--=============================================================================

--! Standard library
library ieee;
--! Standard package
use ieee.std_logic_1164.all;
--! Signed/unsigned calculations
use ieee.numeric_std.all;
--! Math real only for precompliled ports
use ieee.math_real.all;
--! E.g. reduce operations
use ieee.std_logic_misc.all;

--! Xilinx library
library unisim;
--! Xilinx package
use unisim.vcomponents.all;

--! @brief Top entity with a FPGA-independent interface.
entity top is
  port(
    -- System Clk's and Rst
    CPU_RESET         : in std_logic;               --! Reset
    SYSCLK1_300_P     : in std_logic;               --! Clock
    SYSCLK1_300_N     : in std_logic;               --! Clock
    PIN_250MHZ_CLK1_P : in std_logic;               --! User clock
    PIN_250MHZ_CLK1_N : in std_logic;               --! User clock
    -- Status and Debug
    GPIO_DIP_SW : in  std_logic_vector(3 downto 0); --! Dip switch
    GPIO_SW_N   : in  std_logic;                    --! Botton
    GPIO_SW_E   : in  std_logic;                    --! Botton
    GPIO_SW_S   : in  std_logic;                    --! Botton
    GPIO_SW_W   : in  std_logic;                    --! Botton
    GPIO_SW_C   : in  std_logic;                    --! Botton
    GPIO_LED    : out std_logic_vector(7 downto 0); --! LED
    -- SGMII Ethernet LVDS
    PHY1_MDIO        : inout std_logic;             --! Ethernet PHY management data IO
    PHY1_MDC         : out   std_logic;             --! Ethernet PHY management data clock
    PHY1_SGMII_OUT_N : in    std_logic;             --! Ethernet PHY data out 
    PHY1_SGMII_OUT_P : in    std_logic;             --! Ethernet PHY data out
    --PHY1_SGMII_IN_N      : out   std_logic;  --! Ethernet PHY data in
    --PHY1_SGMII_IN_P      : out   std_logic;  --! Ethernet PHY data in
    PHY1_SGMII_CLK_N : in  std_logic;               --! Ethernet PHY clock 625 MHz
    PHY1_SGMII_CLK_P : in  std_logic;               --! Ethernet PHY clock 625 MHz
    PHY1_RESET_B     : out std_logic;               --! Ethernet PHY reset
    -- UART
    USB_UART_TX : in  std_logic;                    --! UART Tx connected device view
    USB_UART_RX : out std_logic;                    --! UART Rx connected device view
    -- GTY
    --MGT233_CLK1_P        : in    std_logic;  --! Differential reference clock inputs MGTREFCLK1
    --MGT233_CLK1_N        : in    std_logic;  --! Differential reference clock inputs MGTREFCLK1
    MGT_SI570_CLOCK3_C_P : in  std_logic;           --! Differential reference clock inputs MGTREFCLK0
    MGT_SI570_CLOCK3_C_N : in  std_logic;           --! Differential reference clock inputs MGTREFCLK0
    FIREFLY_RX1_P        : in  std_logic;           --! Serial data port for transceiver channel; X1Y56_gtyrxp_in;  ch0_gtyrxn_in
    FIREFLY_RX1_N        : in  std_logic;           --! Serial data port for transceiver channel; X1Y56_gtyrxn_in;  ch0_gtyrxp_in
    FIREFLY_TX1_P        : out std_logic;           --! Serial data port for transceiver channel; X1Y56_gtytxp_in;  ch0_gtytxn_in
    FIREFLY_TX1_N        : out std_logic;           --! Serial data port for transceiver channel; X1Y56_gtytxn_in;  ch0_gtytxp_in
    FIREFLY_RX2_P        : in  std_logic;           --! Serial data port for transceiver channel; X1Y57_gtyrxp_in;  ch1_gtyrxn_in
    FIREFLY_RX2_N        : in  std_logic;           --! Serial data port for transceiver channel; X1Y57_gtyrxn_in;  ch1_gtyrxp_in
    FIREFLY_TX2_P        : out std_logic;           --! Serial data port for transceiver channel; X1Y57_gtytxp_in;  ch1_gtytxn_in
    FIREFLY_TX2_N        : out std_logic;           --! Serial data port for transceiver channel; X1Y57_gtytxn_in;  ch1_gtytxp_in
    FIREFLY_RX3_P        : in  std_logic;           --! Serial data port for transceiver channel; X1Y58_gtyrxp_in;  ch2_gtyrxn_in
    FIREFLY_RX3_N        : in  std_logic;           --! Serial data port for transceiver channel; X1Y58_gtyrxn_in;  ch2_gtyrxp_in
    FIREFLY_TX3_P        : out std_logic;           --! Serial data port for transceiver channel; X1Y58_gtytxp_in;  ch2_gtytxn_in
    FIREFLY_TX3_N        : out std_logic;           --! Serial data port for transceiver channel; X1Y58_gtytxn_in;  ch2_gtytxp_in
    FIREFLY_RX4_P        : in  std_logic;           --! Serial data port for transceiver channel; X1Y59_gtyrxp_in;  ch3_gtyrxn_in
    FIREFLY_RX4_N        : in  std_logic;           --! Serial data port for transceiver channel; X1Y59_gtyrxn_in;  ch3_gtyrxp_in
    FIREFLY_TX4_P        : out std_logic;           --! Serial data port for transceiver channel; X1Y59_gtytxp_in;  ch3_gtytxn_in
    FIREFLY_TX4_N        : out std_logic;           --! Serial data port for transceiver channel; X1Y59_gtytxn_in;  ch3_gtytxp_in
    -- DDR4 C1 -- Not connected: DDR4_C1_TEN DDR4_C1_ALERT_B DDR4_C1_PA
    DDR4_C1_ACT_B : out std_logic;
    DDR4_C1_A     : out std_logic_vector (16 downto 0);
    DDR4_C1_BA    : out std_logic_vector (1 downto 0);
    DDR4_C1_BG0   : out std_logic;
    --DDR4_C1_CK_C         : out   std_logic;
    --DDR4_C1_CK_T         : out   std_logic;
    DDR4_C1_CKE     : out   std_logic;
    DDR4_C1_CS_B    : out   std_logic;
    DDR4_C1_DM      : inout std_logic_vector (7 downto 0);
    DDR4_C1_DQ      : inout std_logic_vector (63 downto 0);
    DDR4_C1_DQS_C   : inout std_logic_vector (7 downto 0);
    DDR4_C1_DQS_T   : inout std_logic_vector (7 downto 0);
    DDR4_C1_ODT     : out   std_logic;
    DDR4_C1_RESET_B : out   std_logic );
end entity top;

--! @brief I/O buffer instances common to all FPGAs
architecture struct_top_vcu118 of top is

-- ########################### Constant Definitions ###########################
constant c_BLINK_LED_WL : integer := 32; --! 32 bit counter length

---- ########################### Signal ###########################
---- System Clk's and Rst
signal sl_Clk300_i   : std_logic;                                                        --! Clock in
signal sl_Clk300_buf : std_logic;                                                        --! Clock buffered
signal sl_Clk300     : std_logic;                                                        --! Clock
signal sl_Rst_i      : std_logic;                                                        --! Reset
signal sv_Start300_c : std_logic_vector(c_BLINK_LED_WL - 1 downto 0) := (others => '0'); -- vector for starting up
signal sv_Rst300_c   : std_logic_vector(c_BLINK_LED_WL - 1 downto 0) := (others => '1'); -- vector for starting up reset
-- Miscellaneous signals
signal su_Blink_LED300_c : unsigned(c_BLINK_LED_WL - 1 downto 0) := (others => '0');     --! Counter for blinking LED



begin
-- ########################### IO Buffers & Configuration ###########################
-- System Clk's and Rst
i_Clk300_I_IBUFDS : IBUFDS
  --generic map (IOSTANDARD => "LVDS_25")
  port map(I => SYSCLK1_300_P, IB => SYSCLK1_300_N, O => sl_Clk300_i);
i_Clk300_I_BUFG : BUFG
  port map (I => sl_Clk300_i, O => sl_Clk300_buf);

-- ########################### Port Map ###########################
sl_Rst_i             <= CPU_RESET;
GPIO_LED(3 downto 0) <= std_logic_vector(su_Blink_LED300_c(25 downto 22));

-- ########################### Instantiation ###########################
--...

-- ########################### Processes ###########################
--! @brief Process for blinking led for status of FPGA 300 MHz
p_Sys_Status300 : process(sl_Clk300)
begin
  if rising_edge(sl_Clk300) then
    if sl_Rst_i = '1' then -- synchronous reset (high active)
      su_Blink_LED300_c <= (others => '0');
    else
      su_Blink_LED300_c <= su_Blink_LED300_c + 1;                                -- wrap around by hardware!!
      sv_Start300_c     <= std_logic_vector(su_Blink_LED300_c) or sv_Start300_c; -- vector for starting up
      sv_Rst300_c       <= not sv_Start300_c;                                    -- vector for starting up reset
    end if;
  end if;
end process p_Sys_Status300;

---- ########################### Test section ###########################
--...


end struct_top_vcu118;
