-- libraries
library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all; use ieee.std_logic_textio.all;
-- Standard textIO functions
library std; use std.textio.all;

entity tb_top is
end tb_top;

architecture behavior of tb_top is
	signal CPU_RESET : std_logic;
	signal SYSCLK1_300_P : std_logic;
	signal SYSCLK1_300_N : std_logic;
	signal PIN_250MHZ_CLK1_P : std_logic;
	signal PIN_250MHZ_CLK1_N : std_logic;
	signal GPIO_DIP_SW : std_logic_vector (3 downto 0);
	signal GPIO_SW_N : std_logic;
	signal GPIO_SW_E : std_logic;
	signal GPIO_SW_S : std_logic;
	signal GPIO_SW_W : std_logic;
	signal GPIO_SW_C : std_logic;
	signal PHY1_MDIO : std_logic;
	signal PHY1_SGMII_OUT_N : std_logic;
	signal PHY1_SGMII_OUT_P : std_logic;
	signal PHY1_SGMII_CLK_N : std_logic;
	signal PHY1_SGMII_CLK_P : std_logic;
	signal USB_UART_TX : std_logic;
	signal MGT_SI570_CLOCK3_C_P : std_logic;
	signal MGT_SI570_CLOCK3_C_N : std_logic;
	signal FIREFLY_RX1_P : std_logic;
	signal FIREFLY_RX1_N : std_logic;
	signal FIREFLY_TX1_P : std_logic;
	signal FIREFLY_TX1_N : std_logic;
	signal FIREFLY_RX2_P : std_logic;
	signal FIREFLY_RX2_N : std_logic;
	signal FIREFLY_TX2_P : std_logic;
	signal FIREFLY_TX2_N : std_logic;
	signal FIREFLY_RX3_P : std_logic;
	signal FIREFLY_RX3_N : std_logic;
	signal FIREFLY_TX3_P : std_logic;
	signal FIREFLY_TX3_N : std_logic;
	signal FIREFLY_RX4_P : std_logic;
	signal FIREFLY_RX4_N : std_logic;
	signal FIREFLY_TX4_P : std_logic;
	signal FIREFLY_TX4_N : std_logic;
	signal DDR4_C1_DM : std_logic_vector (7 downto 0);
	signal DDR4_C1_DQ : std_logic_vector (63 downto 0);
	signal DDR4_C1_DQS_C : std_logic_vector (7 downto 0);
	signal DDR4_C1_DQS_T : std_logic_vector (7 downto 0);
	signal GPIO_LED : std_logic_vector (7 downto 0);
	signal PHY1_MDC : std_logic;
	signal PHY1_RESET_B : std_logic;
	signal USB_UART_RX : std_logic;
	signal DDR4_C1_ACT_B : std_logic;
	signal DDR4_C1_A : std_logic_vector (16 downto 0);
	signal DDR4_C1_BA : std_logic_vector (1 downto 0);
	signal DDR4_C1_BG0 : std_logic;
	signal DDR4_C1_CKE : std_logic;
	signal DDR4_C1_CS_B : std_logic;
	signal DDR4_C1_ODT : std_logic;
	signal DDR4_C1_RESET_B : std_logic;

begin

-- Instantiate the Unit Under Test (UUT)
	uut : entity work.top port map (
		CPU_RESET => CPU_RESET,
		SYSCLK1_300_P => SYSCLK1_300_P,
		SYSCLK1_300_N => SYSCLK1_300_N,
		PIN_250MHZ_CLK1_P => PIN_250MHZ_CLK1_P,
		PIN_250MHZ_CLK1_N => PIN_250MHZ_CLK1_N,
		GPIO_DIP_SW => GPIO_DIP_SW,
		GPIO_SW_N => GPIO_SW_N,
		GPIO_SW_E => GPIO_SW_E,
		GPIO_SW_S => GPIO_SW_S,
		GPIO_SW_W => GPIO_SW_W,
		GPIO_SW_C => GPIO_SW_C,
		PHY1_MDIO => PHY1_MDIO,
		PHY1_SGMII_OUT_N => PHY1_SGMII_OUT_N,
		PHY1_SGMII_OUT_P => PHY1_SGMII_OUT_P,
		PHY1_SGMII_CLK_N => PHY1_SGMII_CLK_N,
		PHY1_SGMII_CLK_P => PHY1_SGMII_CLK_P,
		USB_UART_TX => USB_UART_TX,
		MGT_SI570_CLOCK3_C_P => MGT_SI570_CLOCK3_C_P,
		MGT_SI570_CLOCK3_C_N => MGT_SI570_CLOCK3_C_N,
		FIREFLY_RX1_P => FIREFLY_RX1_P,
		FIREFLY_RX1_N => FIREFLY_RX1_N,
		FIREFLY_TX1_P => FIREFLY_TX1_P,
		FIREFLY_TX1_N => FIREFLY_TX1_N,
		FIREFLY_RX2_P => FIREFLY_RX2_P,
		FIREFLY_RX2_N => FIREFLY_RX2_N,
		FIREFLY_TX2_P => FIREFLY_TX2_P,
		FIREFLY_TX2_N => FIREFLY_TX2_N,
		FIREFLY_RX3_P => FIREFLY_RX3_P,
		FIREFLY_RX3_N => FIREFLY_RX3_N,
		FIREFLY_TX3_P => FIREFLY_TX3_P,
		FIREFLY_TX3_N => FIREFLY_TX3_N,
		FIREFLY_RX4_P => FIREFLY_RX4_P,
		FIREFLY_RX4_N => FIREFLY_RX4_N,
		FIREFLY_TX4_P => FIREFLY_TX4_P,
		FIREFLY_TX4_N => FIREFLY_TX4_N,
		DDR4_C1_DM => DDR4_C1_DM,
		DDR4_C1_DQ => DDR4_C1_DQ,
		DDR4_C1_DQS_C => DDR4_C1_DQS_C,
		DDR4_C1_DQS_T => DDR4_C1_DQS_T,
		GPIO_LED => GPIO_LED,
		PHY1_MDC => PHY1_MDC,
		PHY1_RESET_B => PHY1_RESET_B,
		USB_UART_RX => USB_UART_RX,
		DDR4_C1_ACT_B => DDR4_C1_ACT_B,
		DDR4_C1_A => DDR4_C1_A,
		DDR4_C1_BA => DDR4_C1_BA,
		DDR4_C1_BG0 => DDR4_C1_BG0,
		DDR4_C1_CKE => DDR4_C1_CKE,
		DDR4_C1_CS_B => DDR4_C1_CS_B,
		DDR4_C1_ODT => DDR4_C1_ODT,
		DDR4_C1_RESET_B => DDR4_C1_RESET_B
	);

	-- Stimulus process
	stim_proc : process
		file InF: TEXT open READ_MODE is "../../../../../../framework/hdl/tb/vcu118/input_text.txt";
		file OutF: TEXT open WRITE_MODE is "../../../../../../framework/hdl/tb/vcu118/output_text.txt";
		variable ILine: LINE; variable OLine: LINE; variable TimeWhen: TIME;
		variable textio_CPU_RESET,textio_SYSCLK1_300_P,textio_SYSCLK1_300_N,textio_PIN_250MHZ_CLK1_P,textio_PIN_250MHZ_CLK1_N,textio_GPIO_SW_N,textio_GPIO_SW_E,textio_GPIO_SW_S,textio_GPIO_SW_W,textio_GPIO_SW_C,textio_PHY1_MDIO,textio_PHY1_SGMII_OUT_N,textio_PHY1_SGMII_OUT_P,textio_PHY1_SGMII_CLK_N,textio_PHY1_SGMII_CLK_P,textio_USB_UART_TX,textio_MGT_SI570_CLOCK3_C_P,textio_MGT_SI570_CLOCK3_C_N,textio_FIREFLY_RX1_P,textio_FIREFLY_RX1_N,textio_FIREFLY_TX1_P,textio_FIREFLY_TX1_N,textio_FIREFLY_RX2_P,textio_FIREFLY_RX2_N,textio_FIREFLY_TX2_P,textio_FIREFLY_TX2_N,textio_FIREFLY_RX3_P,textio_FIREFLY_RX3_N,textio_FIREFLY_TX3_P,textio_FIREFLY_TX3_N,textio_FIREFLY_RX4_P,textio_FIREFLY_RX4_N,textio_FIREFLY_TX4_P,textio_FIREFLY_TX4_N: bit_vector(0 downto 0);
		variable textio_GPIO_DIP_SW: bit_vector(3 downto 0);
		variable textio_DDR4_C1_DM: bit_vector(7 downto 0);
		variable textio_DDR4_C1_DQ: bit_vector(63 downto 0);
		variable textio_DDR4_C1_DQS_C: bit_vector(7 downto 0);
		variable textio_DDR4_C1_DQS_T: bit_vector(7 downto 0);

	begin
		while not ENDFILE(InF) loop
			READLINE (InF, ILine); -- Read individual lines from input file.
			-- Read from line.
			READ (ILine, TimeWhen);
			READ (ILine, textio_CPU_RESET);
			READ (ILine, textio_SYSCLK1_300_P);
			READ (ILine, textio_SYSCLK1_300_N);
			READ (ILine, textio_PIN_250MHZ_CLK1_P);
			READ (ILine, textio_PIN_250MHZ_CLK1_N);
			READ (ILine, textio_GPIO_DIP_SW);
			READ (ILine, textio_GPIO_SW_N);
			READ (ILine, textio_GPIO_SW_E);
			READ (ILine, textio_GPIO_SW_S);
			READ (ILine, textio_GPIO_SW_W);
			READ (ILine, textio_GPIO_SW_C);
			READ (ILine, textio_PHY1_MDIO);
			READ (ILine, textio_PHY1_SGMII_OUT_N);
			READ (ILine, textio_PHY1_SGMII_OUT_P);
			READ (ILine, textio_PHY1_SGMII_CLK_N);
			READ (ILine, textio_PHY1_SGMII_CLK_P);
			READ (ILine, textio_USB_UART_TX);
			READ (ILine, textio_MGT_SI570_CLOCK3_C_P);
			READ (ILine, textio_MGT_SI570_CLOCK3_C_N);
			READ (ILine, textio_FIREFLY_RX1_P);
			READ (ILine, textio_FIREFLY_RX1_N);
			READ (ILine, textio_FIREFLY_TX1_P);
			READ (ILine, textio_FIREFLY_TX1_N);
			READ (ILine, textio_FIREFLY_RX2_P);
			READ (ILine, textio_FIREFLY_RX2_N);
			READ (ILine, textio_FIREFLY_TX2_P);
			READ (ILine, textio_FIREFLY_TX2_N);
			READ (ILine, textio_FIREFLY_RX3_P);
			READ (ILine, textio_FIREFLY_RX3_N);
			READ (ILine, textio_FIREFLY_TX3_P);
			READ (ILine, textio_FIREFLY_TX3_N);
			READ (ILine, textio_FIREFLY_RX4_P);
			READ (ILine, textio_FIREFLY_RX4_N);
			READ (ILine, textio_FIREFLY_TX4_P);
			READ (ILine, textio_FIREFLY_TX4_N);
			READ (ILine, textio_DDR4_C1_DM);
			READ (ILine, textio_DDR4_C1_DQ);
			READ (ILine, textio_DDR4_C1_DQS_C);
			READ (ILine, textio_DDR4_C1_DQS_T);

		-- insert stimulus here
			wait for TimeWhen - NOW; -- Wait until one time step

			CPU_RESET <= to_stdlogicvector(textio_CPU_RESET)(0);
			SYSCLK1_300_P <= to_stdlogicvector(textio_SYSCLK1_300_P)(0);
			SYSCLK1_300_N <= to_stdlogicvector(textio_SYSCLK1_300_N)(0);
			PIN_250MHZ_CLK1_P <= to_stdlogicvector(textio_PIN_250MHZ_CLK1_P)(0);
			PIN_250MHZ_CLK1_N <= to_stdlogicvector(textio_PIN_250MHZ_CLK1_N)(0);
			GPIO_DIP_SW <= to_stdlogicvector(textio_GPIO_DIP_SW)(3 downto 0);
			GPIO_SW_N <= to_stdlogicvector(textio_GPIO_SW_N)(0);
			GPIO_SW_E <= to_stdlogicvector(textio_GPIO_SW_E)(0);
			GPIO_SW_S <= to_stdlogicvector(textio_GPIO_SW_S)(0);
			GPIO_SW_W <= to_stdlogicvector(textio_GPIO_SW_W)(0);
			GPIO_SW_C <= to_stdlogicvector(textio_GPIO_SW_C)(0);
			PHY1_MDIO <= to_stdlogicvector(textio_PHY1_MDIO)(0);
			PHY1_SGMII_OUT_N <= to_stdlogicvector(textio_PHY1_SGMII_OUT_N)(0);
			PHY1_SGMII_OUT_P <= to_stdlogicvector(textio_PHY1_SGMII_OUT_P)(0);
			PHY1_SGMII_CLK_N <= to_stdlogicvector(textio_PHY1_SGMII_CLK_N)(0);
			PHY1_SGMII_CLK_P <= to_stdlogicvector(textio_PHY1_SGMII_CLK_P)(0);
			USB_UART_TX <= to_stdlogicvector(textio_USB_UART_TX)(0);
			MGT_SI570_CLOCK3_C_P <= to_stdlogicvector(textio_MGT_SI570_CLOCK3_C_P)(0);
			MGT_SI570_CLOCK3_C_N <= to_stdlogicvector(textio_MGT_SI570_CLOCK3_C_N)(0);
			FIREFLY_RX1_P <= to_stdlogicvector(textio_FIREFLY_RX1_P)(0);
			FIREFLY_RX1_N <= to_stdlogicvector(textio_FIREFLY_RX1_N)(0);
			FIREFLY_TX1_P <= to_stdlogicvector(textio_FIREFLY_TX1_P)(0);
			FIREFLY_TX1_N <= to_stdlogicvector(textio_FIREFLY_TX1_N)(0);
			FIREFLY_RX2_P <= to_stdlogicvector(textio_FIREFLY_RX2_P)(0);
			FIREFLY_RX2_N <= to_stdlogicvector(textio_FIREFLY_RX2_N)(0);
			FIREFLY_TX2_P <= to_stdlogicvector(textio_FIREFLY_TX2_P)(0);
			FIREFLY_TX2_N <= to_stdlogicvector(textio_FIREFLY_TX2_N)(0);
			FIREFLY_RX3_P <= to_stdlogicvector(textio_FIREFLY_RX3_P)(0);
			FIREFLY_RX3_N <= to_stdlogicvector(textio_FIREFLY_RX3_N)(0);
			FIREFLY_TX3_P <= to_stdlogicvector(textio_FIREFLY_TX3_P)(0);
			FIREFLY_TX3_N <= to_stdlogicvector(textio_FIREFLY_TX3_N)(0);
			FIREFLY_RX4_P <= to_stdlogicvector(textio_FIREFLY_RX4_P)(0);
			FIREFLY_RX4_N <= to_stdlogicvector(textio_FIREFLY_RX4_N)(0);
			FIREFLY_TX4_P <= to_stdlogicvector(textio_FIREFLY_TX4_P)(0);
			FIREFLY_TX4_N <= to_stdlogicvector(textio_FIREFLY_TX4_N)(0);
			DDR4_C1_DM <= to_stdlogicvector(textio_DDR4_C1_DM)(7 downto 0);
			DDR4_C1_DQ <= to_stdlogicvector(textio_DDR4_C1_DQ)(63 downto 0);
			DDR4_C1_DQS_C <= to_stdlogicvector(textio_DDR4_C1_DQS_C)(7 downto 0);
			DDR4_C1_DQS_T <= to_stdlogicvector(textio_DDR4_C1_DQS_T)(7 downto 0);

			-- Export output state to file.
			write (OLine, TimeWhen);
			write (OLine, string'("  "));
			write (OLine, GPIO_LED(7 downto 0));
			write (OLine, string'("  "));
			write (OLine, PHY1_MDIO);
			write (OLine, string'("  "));
			write (OLine, PHY1_MDC);
			write (OLine, string'("  "));
			write (OLine, PHY1_RESET_B);
			write (OLine, string'("  "));
			write (OLine, USB_UART_RX);
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_ACT_B);
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_A(16 downto 0));
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_BA(1 downto 0));
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_BG0);
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_CKE);
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_CS_B);
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_DM(7 downto 0));
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_DQ(63 downto 0));
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_DQS_C(7 downto 0));
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_DQS_T(7 downto 0));
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_ODT);
			write (OLine, string'("  "));
			write (OLine, DDR4_C1_RESET_B);

			writeline (OutF, OLine); -- write all output variables in file

		end loop;

		wait for 10 NS;
		file_close(InF);
		file_close(OutF);
		wait;
	end process;
end behavior;
