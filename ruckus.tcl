# Load RUCKUS environment and library
 source -quiet $::env(RUCKUS_DIR)/vivado_proc.tcl

 set_property board_part xilinx.com:vcu118:part0:2.3 [current_project]

# set_property ip_repo_paths $::env(IP_REPO) [current_project]
 update_ip_catalog

# Framework and packages
loadSource      -path "$::DIR_PATH/framework/hdl/tb/vcu118/tb_top.vhd" ;# top test bench
loadSource      -path "$::DIR_PATH/framework/hdl/design/vcu118/top.vhd"
loadConstraints -path "$::DIR_PATH/framework/hdl/design/vcu118/vcu118_rev2.0_12082017.xdc"

# Load block design
#loadBlockDesign -path "$::DIR_PATH/framework/hdl/ip/xilinx/bd_mb/block_design.bd"
